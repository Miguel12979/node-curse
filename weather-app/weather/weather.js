const request = require('request');

var getWeather = (latitude, longitude, callback) => {
    
    request({
        url: `https://api.darksky.net/forecast/64de0d6d38f66de3320c4d2738ee231e/${latitude},${longitude}`,
        json: true 
        }, (err, res, body) => {
            if(err){
                callback('Unable to conect to Forecast.io Server');
            }else if(!err && res.statusCode === 200){
                callback(undefined, {
                    temperature: body.currently.temperature,
                    apparentTemperature: body.currently.apparentTemperature
                });
            }else {
                callback('Unable to fetch weather');
            }
        }
    );

}

module.exports = {
    getWeather
};