const expect = require('expect');
const rewire = require('rewire');

var app = rewire('./app');

describe('App', () => {

    var db = {
        saveUser: expect.createSpy()
    };
    app.__set__('db', db);// change function inside app

    it('should call the spy correctly', () => {
        var spy = expect.createSpy();
        spy('Mike', 23);
        expect(spy).toHaveBeenCalledWith('Mike', 23);
    });

    it('should call saveUser with user object', () => {
        var email = 'miguel12979@gmail.com';
        var password = '123456';

       app.handleSignup(email, password);
       expect(db.saveUser).toHaveBeenCalledWith({email, password}); 
    });

});
