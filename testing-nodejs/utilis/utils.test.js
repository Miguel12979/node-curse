const expect = require('expect');
const utils = require("./utils");

describe('Utils', () => {

    describe('#add', () => {
        
        it('should add two numbers', () => {
            var res = utils.add(33, 11);
            expect(res).toBe(44).toBeA('number');
        });

        it('should async add two numbers', (done) => {
            utils.asyncAdd(4, 3, (sum) => {
                expect(sum).toBe(7).toBeA('number');
                    done();
            });
        });
    
    });

    describe('#square', () => {
    
        it('should square a number', () => {
            var res = utils.square(3);

            expect(res).toBe(9).toBeA('number');
        });

        it('should async square a number', (done) => {
            utils.asyncSquare(5, (res) => {
                expect(res).toBe(25).toBeA('number');
                done();
            });
        })
        
    });


});

// it('should expect some values', () => {
    // expect(12).toNotBe(11);
    // expect({name: 'mike'}).toNotEqual({name: 'Mike'});
    // expect([2,3,4]).toExclude(1);
//     expect({name:'Mike',
//         age: 25,
//         location: 'Mexico'
//     }).toExclude({
//         age: 23
//     });
// });

it('should set first and last names', () => {
    var user = { location: 'Mexico', age: 23};
    var res = utils.setName(user, 'Miguel Zablah');

    expect(user).toInclude({
        firstName: 'Miguel',
        lastName: 'Zablah'
    });
});
