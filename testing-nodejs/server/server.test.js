const request = require('supertest');
const expectl = require('expect');

var app = require('./server').app;

describe('Server', () => {
    
    describe('GET /', () => {
        
        it('should return hello world response', (done) => {
            request(app)
                .get('/')
                .expect(404)
                .expect((res) => {
                    expectl(res.body).toInclude({ // Using exprect libary
                        error: 'Page not found.'
                    });
                })
                .end(done);
        });

    });

    describe('GET /Users', () => {
        
        it('should exist user with name Miguel', (done) => {
            request(app)
                .get('/users')
                .expect(200)
                .expect((res) => {
                    expectl(res.body).toInclude({
                        name: 'Miguel',
                        age: 23
                    });
                })
                .end(done);
        });

    });

});


