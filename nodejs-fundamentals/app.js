console.log('Starting app.js');

const fs = require('fs');
const _ = require('lodash');
const yargs = require('yargs');

const notes = require('./notes');

const titleOpt = {
    describe: 'Title of note',
    demand: true,
    alias: 't'
};

const bodyOpt = {
    describe: 'Body of the note',
    demand: true,
    alias: 'b'
};

const argv = yargs
    .command('add', 'Add a new note', {title: titleOpt,body: bodyOpt})
    .command('list', 'List all notes')
    .command('read', 'Read a note', {title: titleOpt})
    .command('remove', 'Removes a note', {title: titleOpt})
    .help()
    .argv;
var command = argv._[0] != null ? argv._[0].toLowerCase() : argv._[0];
// console.log('Command: ', command);
// console.log(argv);

if (command === 'add') {
    var note = notes.addNote(argv.title, argv.body);

    if(note){
        console.log("Note created");
        notes.logNote(note);
    }else{
        console.log("Note title taken");
    }
}else if(command === 'list'){
    var allNotes = notes.getAll();
    console.log(`Printing ${allNotes.length} note(s)`);
    allNotes.forEach(note => notes.logNote(note));
}else if(command === 'read'){
    var note = notes.getNote(argv.title);
    if(note){
        console.log("Note found");
        notes.logNote(note);
    }else{
        console.log("Note not found.");
    }
}else if(command === 'remove'){
    var noteRemoved = notes.removeNote(argv.title);
    var message = noteRemoved ? "Note was removed" : "Note not found";
    console.log(message);
}else{
    console.log('Command not found');
}

