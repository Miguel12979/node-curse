// Arrow function simple example
var square = x => result = x * x;
console.log(square(9));

// Arrow function referend to global 'this' and global 'aguments' not obj or scope 'this' and 'arguments'.
// so if you need to do define 'this' or 'arguments' use the new es6 syntac 
var user = {
    name: 'Andrew',
    // Arrow function no obj literal 'this'
    sayHi: () => {
        console.log(arguments);
        console.log(`Hi I'm ${this.name}`);
    },
    // Es6 obj literal function allows 'this' and 'arguments' (Is NOT a arrow function)
    sayHiAlt() {
        console.log(arguments);
        console.log(`Hi I'm ${this.name}`);
    }
}
// user.sayHi();
user.sayHiAlt(1,2,3,4,5);